<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function tincanapi_verbs() {
    $verbs = array();
    
    $verbs['accepted'] = array(
        'uri' => 'http://activitystrea.ms/schema/1.0/accept',
        'description' => 'Indicates that that the actor has accepted the object. For instance, a person accepting an award, or accepting an assignment.',
        'display' => array(
            'en' => 'Accepted'),
    );
    
    $verbs['completed'] = array(
        'uri' => 'http://activitystrea.ms/schema/1.0/complete',
        'description' => 'Indicates that the actor has completed the object.',
        'display' => array(
            'en' => 'Completed'),
    );
    
    $verbs['experienced'] = array(
        'uri' => 'http://activitystrea.ms/schema/1.0/experience',
        'description' => 'Indicates that the actor has experienced the object in some manner. Note that, depending on the specific object types used for both the actor and object, the meaning of this verb can overlap that of the "consume" and "play" verbs. For instance, a person might "experience" a movie; or "play" the movie; or "consume" the movie. The "experience" verb can be considered a more generic form of other more specific verbs as "consume", "play", "watch", "listen", and "read"',
        'display' => array(
            'en' => 'Experienced'),
    );
    
    return $verbs;
}

function tincanapi_activity() {
    $activity = array();
    
    $activity['event'] = array(
        'uri' => 'http://activitystrea.ms/schema/1.0/event',
        'description' => 'Represents an event that occurs at a certain location during a particular period of time. Objects of this type MAY contain the additional properties specified in Section 3.3.',
        'display' => array(
            'en' => 'Event'),
    );
    
    $activity['Course'] = array(
        'uri' => 'http://adlnet.gov/expapi/activities/course',
        'description' => 'A course represents an entire "content package" worth of material. The largest level of granularity. Unless flat, a course consists of multiple modules. A course is not content.',
        'display' => array(
            'en' => 'Course'),
    );
    
    $activity['Conference'] = array(
        'uri' => 'http://id.tincanapi.com/activitytype/conference',
        'description' => 'A formal meeting which includes presentations or discussions.',
        'display' => array(
            'en' => 'Conference'),
    );
    
    return $activity;
}