<?php

/**
 * Build the configuration form for Tin Can.
 *
 * @return a form array
 */
function tincanapi_admin_settings($form, &$form_state) {
    $form['tincanapi_endpoint_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Endpoint Url'),
        '#default_value' => variable_get('tincanapi_endpoint_url', 'http://'),
        '#description' => t('The endpoint url of the LRS'),
        '#required' => TRUE,
    );

    $form['tincanapi_version'] = array(
        '#type' => 'textfield',
        '#title' => t('Tin Can version'),
        '#default_value' => variable_get('tincanapi_version', '1.0.0'),
        '#description' => t('The version of Tin Can you intend to use'),
        '#required' => TRUE,
    );

    $form['tincanapi_username'] = array(
        '#type' => 'textfield',
        '#title' => t('Username'),
        '#default_value' => variable_get('tincanapi_username', ''),
        '#description' => t('The username used for connecting to the LRS'),
        '#required' => TRUE,
    );

    $form['tincanapi_password'] = array(
        '#type' => 'password',
        '#title' => t('Password'),
        '#default_value' => variable_get('tincanapi_password', '1.0.0'),
        '#description' => t('The password used for connecting to the LRS'),
        '#required' => TRUE,
    );

    return system_settings_form($form);
}
